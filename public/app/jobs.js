/**
 * Created by oleks on 10/01/15.
 */

app.factory('jobs',['$resource', function($resource){
    return $resource('/api/jobs/');
}]);