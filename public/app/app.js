/**
 * Created by oleks on 01/01/15.
 */

var app = angular.module('appJobFinder',['ngResource']);

angular.module('appJobFinder').controller('testCtrl', function($scope, $resource, jobs){
   $scope.jobs = $resource('/api/jobs').query();


   $scope.submit = function(){
      var job = {title: $scope.title, description: $scope.description};

      jobs.save(job);
      $scope.jobs.push(job);
   };
});