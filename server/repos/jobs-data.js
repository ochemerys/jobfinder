/**
 * Created by oleks on 04/01/15.
 */

var mongoose = require('mongoose');
var Promise = require('bluebird');
var jobModel = require('../models/Job');

var jobs = [
    {title: 'Programmer', description: 'Description for Programmer'},
    {title: 'Web Developer', description: 'Description for Web Developer'},
    {title: 'Team Lead', description: 'Description for Team Lead'},
    {title: 'BA', description: 'Description for BA - Business Analyst'},
    {title: 'PM', description: 'Description for PM - Project Manager'}
];

var findJobs = function(query){
    return Promise.cast(mongoose.model('Job').find(query).exec());
};

exports.connectDb = Promise.promisify(mongoose.connect, mongoose);

exports.findJobs = findJobs;

exports.saveJob = jobModel.createJob;

exports.seedJobs = function () {

    return findJobs({}).then(function (collection) {
        if (collection.length === 0) {
            return Promise.map(jobs, function(job){
                return jobModel.createJob(job);
            });
        }
    });
};