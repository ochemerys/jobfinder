/**
 * Created by oleks on 05/01/15.
 */

var bodyParser = require('body-parser');
var validator = require('validator');
var util = require('util');

var errMessage = require('../errorHandling/errorCodeMessageTemplate');

function validateInput(job){
    var errors = [];

    //"ErrorCode": 1001 - range validation error

    //isLength(str, min [, max])
    if(!validator.isLength(job.title,4,40)){
        var errCode = 1001;
        var errField = "title";
        var msg = util.format(errMessage.getTemplate(errCode), errField, 4, 40);

        errors.push({errorCode:errCode, arg: errField, message: msg})
    }
    if(!validator.isLength(job.description,4,250)){
        var errCode = 1001;
        var errField = "description";
        var msg = util.format(errMessage.getTemplate(errCode), errField, 4, 250);

        errors.push({errorCode:errCode, arg: errField, message: msg})
    }
    return errors;
}

module.exports = function(repo, app){

    app.use(bodyParser.json());

    app.get('/api/jobs', function(req, res){
        repo.findJobs().then(function(collection){
            res.send(collection);
        });
    });

    app.post('/api/jobs', function(req, res){
        var job = req.body;

        var validationErrors = validateInput(job);

        if(validationErrors.length > 0){
            res.status(422).send(validationErrors);
        }else{
            repo.saveJob(job, function(err,savedJob){
                res.status(200).send(savedJob);
            });
        }
    });
};
