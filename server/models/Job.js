/**
 * Created by oleks on 03/01/15.
 */

var mongoose = require('mongoose');
var Promise = require('bluebird');

var jobSchema = mongoose.Schema({
    title: {type: String},
    description: {type: String}
});

var Job = mongoose.model('Job', jobSchema);

exports.createJob = Promise.promisify(Job.create, Job);

