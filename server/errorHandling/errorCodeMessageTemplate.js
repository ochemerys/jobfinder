/**
 * Created by oleks on 12/01/15.
 */

var _ = require('underscore');

var messageTemplates = [
    {code: 1001, template: "%s should be in range of %s and %s symbols"},
    {code: 1002, template: ""}
];

exports.getTemplate = function (code) {
   var codeMessage =  _.findWhere(messageTemplates, {code: code});
    return codeMessage.template;
}