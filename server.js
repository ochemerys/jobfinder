/**
 * Created by oleks on 01/01/15.
 */

var express = require('express');
var db = require('./config/database');
var jobsData = require('./server/repos/jobs-data');

var app = express();
require('./server/routes/jobs-service')(jobsData, app);

app.set('views', __dirname + '/server/views');
app.set('view engine', 'jade');

app.use(express.static(__dirname + '/public'));

app.get("*", function (req, res) {
    res.render('index');
});

jobsData.connectDb(db.url)
.then(function () {
    console.log('connected to mongodb successfully');
    jobsData.seedJobs();
});

app.listen(process.env.PORT || 8080);


