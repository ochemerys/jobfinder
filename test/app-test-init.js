/**
 * Created by oleks on 10/01/15.
 */

var app = angular.module('appJobFinder',['ngResource']);

angular.module('appJobFinder').controller('testCtrl', function($scope, $resource){
    $scope.jobs = $resource('/api/jobs').query();
});