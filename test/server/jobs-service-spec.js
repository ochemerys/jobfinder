/**
 * Created by oleks on 05/01/15.
 */

var express = require('express');
var app = express();

var expect = require('chai').expect;
var request = require('supertest');
var Promise = require('bluebird');
var _ = require('underscore');

var dataSavedJob;

var db = {
    findJobs: function () {
        return new Promise(function (resolve, reject) {
            resolve(['hi']);
        });
    },
    saveJob: function (job, next) {
        dataSavedJob = {_id : "54b498007182be3614c506cb", title: job.title, description:job.description} ;
        return next(null,dataSavedJob);
    }
};

var jobService = require('../../server/routes/jobs-service')(db, app);

describe('api: get jobs', function () {

    it('should give me a json list of jobs', function (done) {
        request(app).get('/api/jobs').expect('Content-Type', /json/).end(function (err, res) {
            expect(res.body).to.be.a('Array');
            done();
        });
    });
});

describe('api: save jobs', function () {

    describe('user input', function () {

        describe('title range validation', function () {
            var newJob = {title: 'BA', description: 'this is a description of BA'};

            it('should return status 422 when title is less than 4 characters', function (done) {
                request(app).post('/api/jobs').send(newJob).expect('Content-Type', /json/).end(function (err, res) {
                    expect(res.status).to.equal(422);
                    done();
                });
            });

            it('should return error message "Title should be in range of 4 and 40 symbols" when title is less than 4 characters', function (done) {
                request(app).post('/api/jobs').send(newJob).expect('Content-Type', /json/).end(function (err, res) {
                    var errors = res.body;
                    var error = _.findWhere(errors, {errorCode: 1001, arg: "title"})
                    expect(error.message).to.equal('title should be in range of 4 and 40 symbols');
                    done();
                });
            });

            newJob = {title: '12345678901234567890123456789012345678901', description: 'this is a description of BA'};

            it('should return status 422 when title is more than 40 characters', function (done) {
                request(app).post('/api/jobs').send(newJob).expect('Content-Type', /json/).end(function (err, res) {
                    expect(res.status).to.equal(422);
                    done();
                });
            });

            it('should return error message "Title should be in range of 4 and 40 symbols" when title is more than 40 characters', function (done) {
                request(app).post('/api/jobs').send(newJob).expect('Content-Type', /json/).end(function (err, res) {
                    var errors = res.body;
                    var error = _.findWhere(errors, {errorCode: 1001, arg: "title"})
                    expect(error.message).to.equal('title should be in range of 4 and 40 symbols');
                    done();
                });
            });
        });

        describe('description range validation', function () {
            var newJob = {title: 'Business Analyst', description: 'BA'};

            it('should return status 422 when description is less than 4 characters', function (done) {
                request(app).post('/api/jobs').send(newJob).expect('Content-Type', /json/).end(function (err, res) {
                    expect(res.status).to.equal(422);
                    done();
                });
            });

            it('should return error message "Description should be in range of 4 and 40 symbols" when description is less than 4 characters', function (done) {
                request(app).post('/api/jobs').send(newJob).expect('Content-Type', /json/).end(function (err, res) {
                    var errors = res.body;
                    var error = _.findWhere(errors, {errorCode: 1001, arg: "description"})
                    expect(error.message).to.equal('description should be in range of 4 and 250 symbols');
                    done();
                });
            });

            newJob = {
                title: 'Business Analyst',
                description: '12345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901'
            };

            it('should return status 422 when title is more than 250 characters', function (done) {
                request(app).post('/api/jobs').send(newJob).expect('Content-Type', /json/).end(function (err, res) {
                    expect(res.status).to.equal(422);
                    done();
                });
            });

            it('should return error message "Description should be in range of 4 and 250 symbols" when title is more than 40 characters', function (done) {
                request(app).post('/api/jobs').send(newJob).expect('Content-Type', /json/).end(function (err, res) {
                    var errors = res.body;
                    var error = _.findWhere(errors, {errorCode: 1001, arg: "description"})
                    expect(error.message).to.equal('description should be in range of 4 and 250 symbols');
                    done();
                });
            });
        });

    });


    describe('happy path', function () {

        var newJob = {title: 'Programmer', description: 'Description for Programmer'};

        it('should pass the job to the database save', function (done) {
            request(app).post('/api/jobs').send(newJob).end(function (err, res) {
                expect(dataSavedJob.title).to.equal(newJob.title);
                expect(dataSavedJob.description).to.equal(newJob.description);
                done();
            });

        });

        it('should return the status of 200 to the front end if the database saved', function (done) {
            request(app).post('/api/jobs').send(newJob).expect('Content-Type', /json/).end(function (err, res) {
                expect(res.status).to.equal(200);
                done();
            });
        });

        it('should return the job with an id', function (done) {
            request(app).post('/api/jobs').send(newJob).end(function (err, res) {
                expect(dataSavedJob._id).to.exist;
                done();
            });

        });
    });


    //it('should return the error if database failed'
    ////    , function (done) {
    ////    request(app).post('/api/jobs').send(newJob).end(function (err, res) {
    ////        expect(dataSavedJob._id).to.exist;
    ////        done();
    ////    });
    ////}
    //);

});


